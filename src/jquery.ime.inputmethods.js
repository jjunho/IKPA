( function ( $ ) {
	'use strict';

	$.extend( $.ime.sources, {
		'ikpa': {
			name: 'IKPA',
			source: 'rules/ikpa/ikpa.js'
		},
		'ikpa-sampa': {
			name: 'IKPA (X-SAMPA)',
			source: 'rules/ikpa/ikpa-sampa.js'
		},
		'ipa-sil': {
			name: 'International Phonetic Alphabet - SIL',
			source: 'rules/fonipa/ipa-sil.js'
		},
		'ipa-x-sampa': {
			name: 'International Phonetic Alphabet - X-SAMPA',
			source: 'rules/fonipa/ipa-x-sampa.js'
		}
	} );

	$.extend( $.ime.languages, {
		ikpa: {
			autonym: 'International Korean Phonetic Alphabet',
			inputmethods: [ 'ikpa', 'ikpa-sampa' ]
		},
		fonipa: {
			autonym: 'International Phonetic Alphabet',
			inputmethods: [ 'ipa-x-sampa', 'ipa-sil' ]
		}
	} );

}( jQuery ) );
